package com.example.product.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "product")
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "product_name", length = 50, nullable = false, columnDefinition = "VARCHAR")
    private String productName;

    @Column(name = "product_code", length = 50, nullable = false, columnDefinition = "VARCHAR")
    private String productCode;

    @Column(name = "product_price", nullable = false, columnDefinition = "BIGINT")
    private Long productPrice;

    @Column(name = "store_name", length = 50, nullable = false, columnDefinition = "VARCHAR")
    private String storeName;

    @Column(name = "brand_name", length = 50, nullable = false, columnDefinition = "VARCHAR")
    private String brandName;

    @Column(name = "category_name", length = 50, nullable = false, columnDefinition = "VARCHAR")
    private String categoryName;

    @Column(name = "product_description", nullable = false, columnDefinition = "TEXT")
    private String productDescription;

    public Product() {
    }

    public Product(Long id, String productName, String productCode, Long productPrice, String storeName, String brandName, String categoryName, String productDescription) {
        this.id = id;
        this.productName = productName;
        this.productCode = productCode;
        this.productPrice = productPrice;
        this.storeName = storeName;
        this.brandName = brandName;
        this.categoryName = categoryName;
        this.productDescription = productDescription;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProductName() {
        return this.productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductCode() {
        return this.productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public Long getProductPrice() {
        return this.productPrice;
    }

    public void setProductPrice(Long productPrice) {
        this.productPrice = productPrice;
    }

    public String getStoreName() {
        return this.storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getBrandName() {
        return this.brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getCategoryName() {
        return this.categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getProductDescription() {
        return this.productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }
}