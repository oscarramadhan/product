package com.example.product.Redis.Repository;

import com.example.product.Redis.Entity.RedisProduct;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IRedisProductRepository extends CrudRepository<RedisProduct, Long> {
    RedisProduct findByProductCode(String dataName);
}