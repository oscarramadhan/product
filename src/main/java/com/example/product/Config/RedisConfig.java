package com.example.product.Config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.repository.configuration.EnableRedisRepositories;

import redis.clients.jedis.JedisPoolConfig;

@Configuration
@EnableRedisRepositories
public class RedisConfig{
    @Bean
    public JedisPoolConfig poolConfig() {
        final JedisPoolConfig jedisPoolConfig =  new JedisPoolConfig();         
        jedisPoolConfig.setTestOnBorrow(true);
        jedisPoolConfig.setMaxTotal(10);
        return jedisPoolConfig;
    }
    
    @Bean
    public JedisConnectionFactory redisConnectionFactory() {
        final JedisConnectionFactory connectionFactory = 
            new JedisConnectionFactory( poolConfig() );     
        connectionFactory.setHostName("localhost");
        connectionFactory.setPort(6379);        
        return connectionFactory;
    }

    @Bean
    public RedisTemplate<?, ?> redisTemplate() {
        RedisTemplate<byte[], byte[]> template = new RedisTemplate<>();
        template.setConnectionFactory(redisConnectionFactory());
        return template;

    }

}