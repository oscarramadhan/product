package com.example.product.Service;

import com.example.product.Entity.Product;

public interface IProductService {
    void add(Product product);
    Product getProduct(Long id);
}