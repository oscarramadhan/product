create table `product` (
	`id` bigint(20) not null auto_increment,
	`product_name` varchar(50) not null,
	`product_code` varchar(50) not null,
	`product_price` bigint(20) not null,
	`store_name` varchar(50) not null,
	`brand_name` varchar(50) not null,
	`category_name` varchar(50) not null,
	`product_description` text not null,
	primary key (`id`)
) ENGINE=InnoDB auto_increment=1 default CHARSET=latin1;