package com.example.product.Redis.Entity;

import javax.persistence.Id;

import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.index.Indexed;

@RedisHash("RedisDataset")
public class RedisProduct {
    @Id
    private Long id;

    private String productName;

    @Indexed
    private String productCode;

    private Long productPrice;

    private String storeName;

    private String brandName;

    private String categoryName;

    private String productDescription;

    public RedisProduct() {
    }

    public RedisProduct(Long id, String productName, String productCode, Long productPrice, String storeName, String brandName, String categoryName, String productDescription) {
        this.id = id;
        this.productName = productName;
        this.productCode = productCode;
        this.productPrice = productPrice;
        this.storeName = storeName;
        this.brandName = brandName;
        this.categoryName = categoryName;
        this.productDescription = productDescription;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProductName() {
        return this.productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductCode() {
        return this.productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public Long getProductPrice() {
        return this.productPrice;
    }

    public void setProductPrice(Long productPrice) {
        this.productPrice = productPrice;
    }

    public String getStoreName() {
        return this.storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getBrandName() {
        return this.brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getCategoryName() {
        return this.categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getProductDescription() {
        return this.productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }
}