package com.example.product.Service;

import javax.transaction.Transactional;

import java.util.Optional;

import com.example.product.Entity.Product;
import com.example.product.Redis.Entity.RedisProduct;
import com.example.product.Redis.Repository.IRedisProductRepository;
import com.example.product.Repository.IProductRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Transactional
@Service
public class ProductService implements IProductService {
    @Autowired
    IProductRepository iProductRepository;

    @Autowired
    IRedisProductRepository iRedisProductRepository;

    @Override
    public void add(Product product) {
        iProductRepository.save(product);
        RedisProduct redisProduct = new RedisProduct(
            product.getId(),
            product.getProductName(), 
            product.getProductCode(), 
            product.getProductPrice(),
            product.getStoreName(), 
            product.getBrandName(), 
            product.getCategoryName(), 
            product.getProductDescription()
        );
        iRedisProductRepository.save(redisProduct);
    }

    @Override
    public Product getProduct(Long id) {
        Optional<RedisProduct> redisProduct = iRedisProductRepository.findById(id);
        Product product = null;
        if (redisProduct.isPresent()) {
            System.out.println("redis");
            product = new Product(
                redisProduct.get().getId(), 
                redisProduct.get().getProductName(), 
                redisProduct.get().getProductCode(), 
                redisProduct.get().getProductPrice(), 
                redisProduct.get().getStoreName(), 
                redisProduct.get().getBrandName(), 
                redisProduct.get().getCategoryName(), 
                redisProduct.get().getProductDescription()
            );
        } else {
            System.out.println("db");
            Optional<Product> dbProduct = iProductRepository.findById(id);
            if (dbProduct.isPresent()) {
                product = dbProduct.get();

                RedisProduct newRedisProduct = new RedisProduct(
                    product.getId(),
                    product.getProductName(), 
                    product.getProductCode(), 
                    product.getProductPrice(),
                    product.getStoreName(), 
                    product.getBrandName(), 
                    product.getCategoryName(), 
                    product.getProductDescription()
                );
                iRedisProductRepository.save(newRedisProduct);
            }
        }
        return product;
    }

}