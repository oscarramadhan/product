package com.example.product.Controller;

import javax.validation.Valid;

import com.example.product.Entity.Product;
import com.example.product.Service.IProductService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/product")
public class ProductController {
    @Autowired
    IProductService iProductService;

    @RequestMapping(value = "add", method = RequestMethod.POST)
    public ResponseEntity<?> add(@RequestBody @Valid Product product){
        iProductService.add(product);
        return new ResponseEntity<>("success", HttpStatus.OK);
    }

    @RequestMapping(value = "get/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> get(@PathVariable @Valid Long id){
        Product product = iProductService.getProduct(id);
        return new ResponseEntity<>(product, HttpStatus.OK);
    }
}